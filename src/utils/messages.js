export default {
    'login': 'Для начала необходимо войти в систему',
    'logout': 'Вы вышли из системы',
    'auth/user-not-found': 'Пользователя с каким Email не существует',
    'auth/wrong-password': 'Введен неправильный пароль',
    'auth/email-already-in-use': 'Пользователь с таким Email уже существует'
}